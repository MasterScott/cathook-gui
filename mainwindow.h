#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QTextEdit>
#include <memory>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private slots:
  void on_updateButton_clicked();
  void readTerminalOutput();
  void readUpdateTerminalOutput();
  void closeTerminalProcess();

  void on_injectButton_clicked();

  void on_configButton_clicked();

  void on_preloadButton_clicked();

private:
  Ui::MainWindow *ui;
  std::unique_ptr<QProcess> process;
  void setTextTermFormatting(QTextEdit *textEdit, QString const &text);
  void parseEscapeSequence(int attribute, QListIterator<QString> &i,
                           QTextCharFormat &textCharFormat,
                           QTextCharFormat const &defaultTextCharFormat);
  void updateButtons(bool enable);
  void checkUpdates();
};

#endif // MAINWINDOW_H
